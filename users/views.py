
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib import messages

from users.forms import LoginForm


def index(request):
    storage = messages.get_messages(request)
    bad_credentials = False
    for message in storage:
        bad_credentials = message
        break
    return render(request, "users/login.html", context={'bad_credentials': bad_credentials})


def register_view(request):
    if request.method == 'POST':
        user = User.objects.create_user(request.POST['username'], '', request.POST['password'])
        user.save()
        login(request, user)
        return HttpResponseRedirect('/crop')
    else:
        return render(request, "users/signup.html")


def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                if request.GET.get('next'):
                    if request.GET['next'] != '/logout/':
                        return HttpResponseRedirect(request.GET['next'])
                return HttpResponseRedirect('/crop')
            else:
                return render(request, "users/login.html", context={'bad_credentials': messages.WARNING})
    elif request.user.is_authenticated:
        return HttpResponseRedirect('/crop')
    else:
        form = LoginForm()
    return render(request, 'users/login.html', {"form": form})


@login_required
def logout_view(request):
    logout(request)
    return HttpResponseRedirect('')
