from django.apps import AppConfig


class CroppingServiceConfig(AppConfig):
    name = 'cropping_service'
