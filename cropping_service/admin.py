from django.contrib import admin

from cropping_service.models import TrainingImage


class TrainingImageAdmin(admin.ModelAdmin):
    list_display = ('id', 'number',)


admin.site.register(TrainingImage, TrainingImageAdmin)
