import numpy as np
from django.conf import settings
from django.core.files import File
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render

from cropping_service.models import TrainingImage
from django.db.models import Q
import os
from threading import Lock
from django.core import serializers

mutex = Lock()


def index(request):
    if request.method == 'POST':
        if request.POST['button'] == 'delete':
            return delete_image(request)
        if verify_number(request.POST['number']):
            TrainingImage.objects.filter(id=request.POST['id']).update(number=request.POST['number'])
    return get_next_image(request)


def delete_image(request):
    TrainingImage.objects.filter(id=request.POST['id']).delete()
    return get_next_image(request)


def get_image(request, image_id):
    image = TrainingImage.objects.all().filter(pk=image_id)
    if image:
        serialized_queryset = serializers.serialize('json', image)
        return JsonResponse(serialized_queryset, safe=False)
    return HttpResponse(status=404)


def get_next_image(request):
    mutex.acquire()
    image = TrainingImage.objects.filter(Q(number='') & (Q(moderator__isnull=True) | Q(moderator=request.user))).first()
    TrainingImage.objects.filter(id=image.id).update(moderator=request.user)
    mutex.release()
    return render(request, 'cropping_service/index.html', {'id': image.id,
                                                           'image_url': '/media' + image.image.path.split('/media')[1]})


def verify_number(number):
    if len(number) != 8:
        return False
    digits = np.array([int(digit) for digit in number])
    coefficients = np.array([2, 1, 2, 1, 2, 1, 2])
    vector = digits[:-1] * coefficients
    s = sum([int(digit) for digit in ''.join(str(item) for item in vector)]) + digits[-1]
    return s % 10 == 0


def load_from_folder(request):
    folder = request.GET['folder']

    for filename in os.listdir(folder):
        if filename.endswith('.jpg'):
            image = TrainingImage.objects.create()
            image.image.save(
                os.path.join(settings.MEDIA_ROOT, 'images', filename),
                File(open(os.path.join(folder, filename), "rb"))
            )
            image.save()
    return HttpResponse(200)
