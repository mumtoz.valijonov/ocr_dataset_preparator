from django.contrib.auth.models import User
from django.db import models


class TrainingImage(models.Model):

    image = models.ImageField(upload_to='images/', unique=True)
    number = models.CharField(max_length=8, blank=True, default='')
    moderator = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return str(self.id)

    def create_xml(self):
        pass
