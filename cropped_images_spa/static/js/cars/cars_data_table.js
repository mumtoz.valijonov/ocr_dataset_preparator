    window.addEventListener("load", function(event) {
        var table = $('#cars_datatables').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true,
            "columns": [
                { "data": 1, "render": function(data, type, row) {
                    return "<div class='popup' onclick='" + "showPopup(" + row[0] + ")'>" + data +
                              "<span class='popuptext' id='" + "carDetailsPopup" + row[0] + "'>" + "" + "</span>" +
                            "</div>";
                    }
                },
                { "orderable": false, "data": 3 },
                { "data": 4 },
                { "data": 5 },
                { "orderable": false, "render": (data, type, row) => (row[7] == 'True' ? '<span class="text-danger">' : '<span>') + row[6] + '</span>' },
                {
                    "orderable": false, "data": "action" , "render": function ( data, type, row ) {
                    return "{% if user.is_superuser %}<a href='/cars/create?id="+row[0]+"' class='btn btn-success btn-xs'>Изменить</a>{% endif %}" +
                        "&nbsp;<a href='/cars/view?id="+row[0]+"' class='btn btn btn-primary btn-xs'>Обзор</a>" +
                        "&nbsp;{% if user.is_superuser %}<a href='/cars/delete?id="+row[0]+"' class='btn btn-danger btn-xs'>Удалить</a>{% endif %}";
                    }
                }
            ],
            "ajax": {
                "url": "{% url 'cars_list_json' %}",
                "type": "POST",
                "contentType": "application/json",
                "dataType": 'json',
                "headers": {
                   'Content-Type': 'application/json',
                   'Accept': 'application/json',
                   'X-CSRFToken':getCookie("csrftoken")
                },
                "data": function (json) {
                   return JSON.stringify(json);
                }
            }
        });

        $('#dropdown1').on('change', function () {
            table.columns(1).search( this.value ).draw();
        });
    });

    function showPopup(id) {
        var popup = document.getElementById("carDetailsPopup" + id);
        console.log(popup.classList);
        if (popup.classList.length == 1) {
            var text = httpGet("/cars/details?id=" + id);
            console.log("Text: " + text);
            popup.innerHTML = text;
        }
        popup.classList.toggle("show");
    }

    function httpGet(url) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open( "GET", url, false );
        xmlHttp.send( null );
        return xmlHttp.responseText;
    }